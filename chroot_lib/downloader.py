import urllib

class DownloaderException(Exception):
    pass

class Downloader(object):

    def __init__(self, url, destination='/tmp/'):
        self.url = url
        self.destination = destination

    def download(self):
        """ Download a resource at `url` and save it in
        the destination directory
        """
        write_location = self.destination + self.get_filename()
        try:
            urllib.urlretrieve(self.url, write_location)
        except IOError:
            raise DownloaderException("Could not download information at URL")

        return write_location

    def get_filename(self):
        """ Get filename at the end of a url """
        return self.url.split('/')[-1]

