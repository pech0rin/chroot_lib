""" Class used to decompress a file to a given location """

import os
import tarfile
import zipfile

class DecompressorException(Exception):
    pass

class Decompressor(object):

    def __init__(self, file_location, dest_path="/tmp/"):
        self.file_location = file_location
        self.dest_path = dest_path

    def decompress(self):
        """ Decompress a file to a specific location """
        if tarfile.is_tarfile(self.file_location):
            self.extract_tar()
        elif zipfile.is_zipfile(self.file_location):
            self.extract_zip()
        else:
            raise DecompressorException("Unsupported compression format: supported formats are zip and tar")

    def extract_tar(self):
        tar = tarfile.open(self.file_location)
        self._extract(tar)

    def extract_zip(self):
        zfile = zipfile.ZipFile(self.file_location)
        self._extract(zfile)

    def _extract(self, file_object):
        self._check_location()
        file_object.extractall(path=self.dest_path)
        file_object.close()

    def _check_location(self):
        if not os.path.exists(self.dest_path):
            os.mkdir(self.dest_path)
        else:
            raise DecompressorException("Decompressed folder already exists")

