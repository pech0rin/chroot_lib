""" Library to implement a simple chroot environment.
It has the ability to download images, create chroot enviroments
from those images, and run tasks in those chroots

"""

from .downloader import Downloader, DownloaderException
from .decompressor import Decompressor, DecompressorException
from .chroot import Chroot, ChrootException

import os
import uuid

def send_signal(pid, signal):
    """ Send a signal to a running process """
    # +1 is for child process since parent process is a shell
    pid += 1 # TODO Add better method for optaining child PID
    os.kill(pid, signal)

def chroot_from_url(url, chroot_location):
    """ Get a chroot enviroment from an image url """
    folder = get_decompressed_folder(url, chroot_location)
    return Chroot(folder)

def get_decompressed_folder(url, chroot_location):
    downloader = Downloader(url)
    file_loc = downloader.download()
    new_folder_name = get_folder_name(chroot_location)

    Decompressor(file_loc, new_folder_name).decompress()

    os.remove(file_loc)
    return new_folder_name


def get_folder_name(root_loc):
    root_name = str(uuid.uuid4())
    return root_loc + root_name
