""" Run a task in a chroot

Escaping chroot logic grabbed from here: http://nessy.info/?p=306
"""

import os
import sys
import subprocess

class ChrootException(Exception):
    pass


class Chroot(object):
    """ Execute a command in a chroot """

    def __init__(self, root_path):
        self.root_path = root_path

    def execute(self, command):
        try:
            root = os.open("/", os.O_RDONLY)
            os.chroot(self.root_path)

            return self._run_command(command)
        finally:
            os.fchdir(root)
            os.chroot(".")


    def _run_command(self, command):
        try:
            process = subprocess.Popen(
                command,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                shell=True,
                close_fds=True,
                bufsize=1
            )
            return Process(process, command)
        except OSError as e:
            raise ChrootException(e.message)



class Process(object):
    """ Simple wrapper for popen process """

    def __init__(self, process, command):
        self.process = process
        self.command = command

    def print_output(self):
        """ Setup queue and thread to continously read output
        Adapted from: http://stackoverflow.com/questions/18421757/live-output-from-subprocess-command
        """
        print "**STDOUT**:"
        for c in iter(lambda: self.process.stdout.read(1), ''):
            sys.stdout.write(c)

        print "**STDERR**:"
        for c in iter(lambda: self.process.stderr.read(1), ''):
            sys.stderr.write(c)

    def pid(self):
        return self.process.pid

