""" Client for interfacing with chroot lib

***Usage:***

Run a command:

sudo python pychroot.py --run http://localhost/image.tar ls /etc


Send a signal:

sudo python pychroot.py --signal proc1 9

Check if healthy:

sudo python pychroot.py --healthy proc1

"""

from chroot_lib import (
    chroot_from_url,
    send_signal,
    DownloaderException,
    DecompressorException,
    ChrootException
)

import sys
import os
import pickle
import argparse

class Pychroot(object):

    PICKLE_LOC = "/etc/pychroot/process.pkl"

    def __init__(self, chroot_loc="/etc/pychroot/"):
        self.processes = self.load_processes()
        self.chroot_loc = chroot_loc

    def load_processes(self):
        """ Load processes names mapped to objects,
        stored in a pickle file.
        """
        if os.path.isfile(self.PICKLE_LOC):
            with open(self.PICKLE_LOC, 'rb') as pickle_file:
                return pickle.load(pickle_file)
        return {}

    def run_task(self, url, command, name=None):
        chroot = self._get_chroot(url)
        p = chroot.execute(command)

        if name is None:
            proc_num = len(self.processes.keys()) + 1
            name = "proc" + str(proc_num)

        self.processes[name] = p.pid()
        self.save()
        self.print_process(p, name)

    def save(self):
        """ Pickle the process dictionary """
        with open(self.PICKLE_LOC, 'wb') as output:
            pickle.dump(self.processes, output)

    def print_process(self, process, name):
        """ Print the process and its std_in/out to console """
        text = "Process Name: " + name
        print "{:*^50}".format(text)
        process.print_output()

    def _get_chroot(self, url):
        try:
            chroot = chroot_from_url(url, self.chroot_loc)
        except DownloaderException as e:
            error = "Error downloading file: {}".format(e.message)
            error += "\nPlease check to make sure the URL is correct"
            sys.exit(error)
        except DecompressorException as e:
            error = "Error decompressing file: {}".format(e.message)
            error += "\nPlease check to make sure file is of correct type"
            sys.exit(error)
        return chroot

    def check_alive(self, name):
        """ Check if a process is alive or not """
        try:
            self.send_signal(name, 0)
            print "Process {} is healthy!".format(name)
        except:
            print "Process {} is dead :(".format(name)

    def send_signal(self, name, signal):
        """ Send a signal to a named process """
        try:
            pid = self.processes[name]
            send_signal(pid, signal)
        except KeyError:
            sys.exit("Could not find process with name: {}".format(name))
        except OSError:
            sys.exit("No process found on system")


if __name__ == "__main__":
    pychroot = Pychroot()

    # Parse Arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--healthy')
    parser.add_argument('--signal', nargs="*")
    parser.add_argument('--run')
    parser.add_argument('command', nargs=argparse.REMAINDER)

    args = vars(parser.parse_args())

    if args['run']:
        image_url = args['run']
        command = ' '.join(args['command'])
        pychroot.run_task(image_url, command)

    elif args['signal']:
        name = args['signal'][0]
        signal = int(args['signal'][1])
        pychroot.send_signal(name, signal)

    elif args['healthy']:
        pychroot.check_alive(args['healthy'])
