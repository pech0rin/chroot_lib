# chroot_lib

This is a simple python library that allows users to download images and run
tasks inside of them. A test client named `pychroot.py` is also included to
show the library capabilities.

## Client Usage

### Run a task

This will print out the std_out and std_err of the command. Or, if any errors were encountered, the error messages.
Additionally, a process name will be printed out (example `proc1` or `proc2`), you can use this name to send
signals to the running processes.

```shell
sudo python pychroot.py --run https://localhost/image.tar ls /etc
```

### Send a signal

This will send a signal to any currently running processes started using `pychroot`. Use the name of the process returned
from the `run` command to send an integer signal to the process.

```shell
sudo python pychroot.py --signal proc10 9
```

### Check if healthy

This will check to see if a process is healthy and running or dead.

```shell
sudo python pychroot.py --healthy proc1
```


## Library Installation

```shell
python setup.py install
```


## Library Usage

### Download an image

```python

from chroot_lib import Downloader

Downloader("http://www.yourimage.com/image.tar", "/tmp/my-image-location").download()
```

### Decompress an image

```python

from chroot_lib import Decompressor

Decompressor("/tmp/my-image-location/image.tar", "/tmp/uncompressed").decompress()
```

### Run a chroot task in a directory

```python

from chroot_lib import Chroot

chroot = Chroot("/path/to/new/root")
process = chroot.execute("ls -la /")
process.print_output()
```

### Convenience Functions

Create a Chroot object from a url to an image:

```python

from chroot_lib import chroot_from_url

chroot = chroot_from_url("https://me.com/image.tar", "/tmp/my-new-chroot/")
process = chroot.execute("ls -la /")
```

## Running the tests

The tests use py.test and are located in the test.py file.

To install py.test:

```shell
pip install pytest
```

To run the tests:

```shell
py.test test.py
```

