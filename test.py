""" Tests for chroot library """

from chroot_lib import Downloader, DownloaderException
from chroot_lib import Decompressor, DecompressorException
from chroot_lib import Chroot, ChrootException

import os
import shutil

filename = "apk-tools-static-2.4.4-r0.apk"
image_url = "http://nl.alpinelinux.org/alpine/v3.0/main/x86_64/" + filename
decompress_loc = "/tmp/chroot_testing"

def test_image_filename():
    d = Downloader(image_url)
    assert d.get_filename() == filename


def test_image_download():
    download_location = Downloader(image_url).download()

    assert os.path.isfile(download_location)

    os.remove(download_location)

def test_bad_url_download():
    try:
        Downloader("htp://wrong.com").download()
        assert False
    except DownloaderException as e:
        assert e.message == "Could not download information at URL"


def test_empty_url_download():
    try:
        Downloader("http://").download()
        assert False
    except DownloaderException as e:
        assert e.message == "Could not download information at URL"


def test_decompression_error():
    try:
        decompress_setup("http://www.fullstackpython.com/flask.html")
        assert False
    except DecompressorException as e:
        assert e.message == "Unsupported compression format: supported formats are zip and tar"


def test_image_decompression():
    decompress_setup(image_url)

    assert os.path.isdir(decompress_loc)
    assert os.path.isdir(decompress_loc + "/sbin")

    decompress_teardown()


def test_zipfile_decompression():
    zip_url = "http://www.colorado.edu/conflict/peace/download/peace.zip"
    decompress_setup(zip_url)

    assert os.path.isdir(decompress_loc)
    assert os.path.isfile(decompress_loc + "/survey.htm")

    decompress_teardown()

def test_chroot_tasks():
    chroot = get_chroot()

    p1 = chroot.execute("echo \"WORLD\"")
    p2 = chroot.execute("echo \"HELLO\"")
    assert p1.command == "echo \"WORLD\""
    assert p2.command == "echo \"HELLO\""

    p3 = chroot.execute("unknowncommand")
    assert p3.command == "unknowncommand"

    decompress_teardown()

def get_chroot():
    decompress_file_setup("./test_resources/rootfs.tar")
    return Chroot(decompress_loc)


def decompress_setup(url):
    download_location = Downloader(url).download()
    try:
        decompress_file_setup(download_location)
    except DecompressorException as e:
        if e.message != "Decompressed folder already exists":
            raise e

def decompress_file_setup(file_loc):
    Decompressor(file_loc, decompress_loc).decompress()


def decompress_teardown():
    shutil.rmtree(decompress_loc, ignore_errors=True)
