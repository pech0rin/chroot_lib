""" Setup for chroot_lib """

from setuptools import setup, find_packages

setup(
    name="chroot_lib",
    version="0.0.1",
    description="Simple python library for chroot manipulation",
    url="https://bitbucket.org/pech0rin/chroot_lib.git",
    author="Sam Johnson",
    author_email="sjohnson540@gmail.com",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Libraries',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],
    packages=find_packages()
)
